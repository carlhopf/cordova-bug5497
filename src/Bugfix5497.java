package com.carlhopf.cordova.plugin.bugfix5497;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.apache.cordova.PluginResult.Status;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import android.util.Log;
import android.graphics.Rect;
import android.widget.FrameLayout;
import android.app.Activity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewGroup;


public class Bugfix5497 extends CordovaPlugin {
    private static final String TAG = "Bugfix5497";
    private SoftInputAssist mAssist = null;

    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        final Activity activity = cordova.getActivity();
        
        mAssist = new SoftInputAssist(activity); 

        /*
        activity.runOnUiThread(new Runnable(){
            @Override
            public void run() {
                AndroidBug5497Workaround.assistActivity(activity);
            }
        });
        */
    }

    @Override
    public boolean execute(String action, JSONArray inputs, CallbackContext callbackContext) throws JSONException {
        return false;
    }
    
    @Override
    public void onPause(boolean multitasking) {
        mAssist.onPause();
    }
    
    @Override
    public void onResume(boolean multitasking) {
        mAssist.onResume();
    }
    
    @Override
    public void onDestroy() {

    }
    
    /**
     * https://stackoverflow.com/a/42261118
     * Works better than AndroidBug5497Workaround, no gap between content and keyboard.
     */
    private static class SoftInputAssist {
        private View rootView;
        private ViewGroup contentContainer;
        private ViewTreeObserver viewTreeObserver;
        private ViewTreeObserver.OnGlobalLayoutListener listener = () -> possiblyResizeChildOfContent();
        private Rect contentAreaOfWindowBounds = new Rect();
        private FrameLayout.LayoutParams rootViewLayout;
        private int usableHeightPrevious = 0;
    
        public SoftInputAssist(Activity activity) {
            contentContainer = (ViewGroup) activity.findViewById(android.R.id.content);
            rootView = contentContainer.getChildAt(0);
            rootViewLayout = (FrameLayout.LayoutParams) rootView.getLayoutParams();
        }
    
        public void onPause() {
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeOnGlobalLayoutListener(listener);
            }
        }
    
        public void onResume() {
            if (viewTreeObserver == null || !viewTreeObserver.isAlive()) {
                viewTreeObserver = rootView.getViewTreeObserver();
            }
    
            viewTreeObserver.addOnGlobalLayoutListener(listener);
        }
    
        public void onDestroy() {
            rootView = null;
            contentContainer = null;
            viewTreeObserver = null;
        }
    
        private void possiblyResizeChildOfContent() {
            contentContainer.getWindowVisibleDisplayFrame(contentAreaOfWindowBounds);
            int usableHeightNow = contentAreaOfWindowBounds.height();
    
            if (usableHeightNow != usableHeightPrevious) {
                rootViewLayout.height = usableHeightNow;
                rootView.layout(contentAreaOfWindowBounds.left, contentAreaOfWindowBounds.top, contentAreaOfWindowBounds.right, contentAreaOfWindowBounds.bottom);
                rootView.requestLayout();
    
                usableHeightPrevious = usableHeightNow;
            }
        }
    }



    /**
     * http://stackoverflow.com/questions/7417123/android-how-to-adjust-layout-in-full-screen-mode-when-softkeyboard-is-visible/19494006#19494006
     *
     * Now can use android:theme="@android:style/Theme.NoTitleBar.Fullscreen" with android:windowSoftInputMode="adjustResize"
     * and it will correctly resize when softinput opened by WebView.
     */
    private static class AndroidBug5497Workaround {
        // For more information, see https://code.google.com/p/android/issues/detail?id=5497
        // To use this class, simply invoke assistActivity() on an Activity that already has its content view set.

        public static void assistActivity (Activity activity) {
            new AndroidBug5497Workaround(activity);
        }

        // view added by setContentView()
        private View mSetContentView;

        // topmost view for acitivity (its a PhoneDecorView), always takes full height of window
        private View mRootView;

        private int usableHeightPrevious, fullHeightPrevious;
        private FrameLayout.LayoutParams setContentViewParams;

        private AndroidBug5497Workaround(Activity activity) {
            // parent/base (always FrameLayout) of view added by setContentView()
            FrameLayout content = (FrameLayout) activity.findViewById(android.R.id.content);
            mSetContentView = content.getChildAt(0);
            mRootView = content.getRootView();

            mSetContentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                public void onGlobalLayout() {
                    possiblyResizeChildOfContent();
                }
            });

            setContentViewParams = (FrameLayout.LayoutParams) mSetContentView.getLayoutParams();
        }

        // if height changed,
        private void possiblyResizeChildOfContent() {
            int usableHeight = computeUsableHeight();
            int fullHeight = getRootViewHeight();
            Log.i(TAG, "usable height " + usableHeight + " prev " + usableHeightPrevious + " fullHeight " + fullHeight);

            // visible screen height, or height minus keyboard changed
            if (usableHeight != usableHeightPrevious || fullHeight != fullHeightPrevious) {
                int heightDifference = fullHeight - usableHeight;
                if (heightDifference > (fullHeight / 4)) {
                    // keyboard probably just became visible
                    //setContentViewParams.height = fullHeight - heightDifference;
                    setContentViewParams.setMargins(0, 0, 0, heightDifference);
                } else {
                    // keyboard probably just became hidden
                    //setContentViewParams.height = fullHeight;
                    setContentViewParams.setMargins(0, 0, 0, 0);
                }

                mSetContentView.requestLayout();
                usableHeightPrevious = usableHeight;
                fullHeightPrevious = fullHeight;
            }
        }

        // tells you the available area where content can be placed and remain visible to users
        // (screenHeight - keyboard - bottomNavi - notificationBar)
        private int computeUsableHeight() {
            Rect r = new Rect();
            mSetContentView.getWindowVisibleDisplayFrame(r);
            return (r.bottom - r.top);
        }

        // getRootView() is the 
        private int getRootViewHeight() {
            return mRootView.getHeight();
        }
    }
}
