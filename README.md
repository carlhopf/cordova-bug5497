android bugs
-------------

- adjustResize doesn't work in fullscreen mode
- adjustPan doesnt work in webview (non- and fullscreen)
- hiding status bar can only be done by enabling fullscreen mode

solution
--------

detect available height change, and accordingly update cordova activity rootView height

